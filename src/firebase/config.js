import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyCkmkVhRvRoBOqCFEGpZVOfW_8aXL8VilY',
  databaseURL: 'https://travel-f6cdc-default-rtdb.firebaseio.com',
  projectId: 'travel-f6cdc',
  storageBucket: 'travel-f6cdc.appspot.com',
  appId: '1:195058423148:ios:9f59a9d067faf360a6cf49',
};

let app;

if (firebase.apps.length === 0) {
  app = firebase.initializeApp(firebaseConfig)
} else {
  app = firebase.app();
}

export { firebase };