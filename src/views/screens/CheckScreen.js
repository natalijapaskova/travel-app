import React, { useState } from 'react'
import { Image, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './stylesLogin';

import { firebase } from '../../firebase/config'


export default function CheckScreen({navigation, route}) {

    const entities=route.params;
    const [password, setPassword] = useState('')

   
    const onLoginPress = () => {
        if(password=='admin'){
            navigation.navigate('UpdateScreen', entities)
        }
        else {
            alert("Wrong password for admin")
        }

    }


    return (
        <View style={styles.container}>
            <KeyboardAwareScrollView
                style={{ flex: 1, width: '100%' }}
                keyboardShouldPersistTaps="always">

<Image
                    style={styles.logo}
                    source={require('../../assets/admin.jpg')}
                />
                <TextInput
                    style={styles.input}
                    placeholderTextColor="#aaaaaa"
                    secureTextEntry
                    placeholder='Password'
                    value={password}
                    onChangeText={(text) => setPassword(text)}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                />
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => onLoginPress()}>
                    <Text style={styles.buttonTitle}>Log in</Text>
                </TouchableOpacity>
            </KeyboardAwareScrollView>
        </View>
    )
}