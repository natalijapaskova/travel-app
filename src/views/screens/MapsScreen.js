import * as React from 'react';
import MapView, { Marker } from "react-native-maps";
import { StyleSheet, Text, View, Dimensions, Image } from 'react-native';


const MapsScreen = ({navigation, route}) => {

    const place=route.params;

        return (
            <View style={styles.container}>
                <MapView style={styles.map}
                    initialRegion={{
                        latitude: 46.5547,
                        longitude: 15.6459,
                        latitudeDelta: 0.05,
                        longitudeDelta: 0.05
                    }}
                >
                    <MapView.Marker
                        coordinate={{
                            latitude: 46.5574941,
                            longitude: 15.6412016
                        }}
                        title={"Hotel1"}
                        description={"Vodnikov trg"}
                    >
                        <Image source={require('../../assets/hotel.png')} style={{ height: 35, width: 35 }} />
                    </MapView.Marker>
                    <MapView.Marker
                        coordinate={{
                            latitude: 46.5610891,
                            longitude: 15.6289295
                        }}
                        title={"Restaurant1"}
                        description={"Address"}
                    >
                        <Image source={require('../../assets/restaurant.jpg')} style={{ height: 35, width: 35 }} />
                    </MapView.Marker>
                </MapView>
            </View>
        );
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    map: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
});

export default MapsScreen;