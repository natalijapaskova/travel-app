import React, { useState } from 'react'
import { Image, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './stylesRegistration';

import { firebase } from '../../firebase/config'


export default function UpdateScreen({ navigation, route }) {

    const entities = route.params;
    const entityRef = firebase.firestore().collection('places')

       
    //const city=entities.city;
    const onAddButtonPress = () => {

            const timestamp = firebase.firestore.FieldValue.serverTimestamp();
            const data = {
                city: city,
                country: country,
                description: description
            };
            entityRef
                .doc(entities.id).set(data)
                .then(() => {
                    navigation.navigate('HomeScreen')
                })
                .catch((error) => {
                    alert(error)
                });
        
    }
    const [city, setCity] = useState(entities.city)
    const [country, setCountry] = useState(entities.country)
    const [description, setDescription] = useState(entities.description)
    const [population, setPopulation] = useState(entities.population)

    

    return (
        <View style={styles.container}>
            <KeyboardAwareScrollView
                style={{ flex: 1, width: '100%' }}
                keyboardShouldPersistTaps="always">
                <Image
                    style={styles.logo}
                    source={require('../../assets/admin.jpg')}
                />
                <TextInput
                    style={styles.input}
                    placeholder='Full Name'
                    placeholderTextColor="#aaaaaa"
                    onChangeText={(text) => setCity(text)}
                    value={city}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                />
                <TextInput
                    style={styles.input}
                    placeholder='E-mail'
                    placeholderTextColor="#aaaaaa"
                    onChangeText={(text) => setCountry( text) }
                    value={country}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                />
                 <TextInput
                    style={styles.input2}
                    placeholder='E-mail'
                    placeholderTextColor="#aaaaaa"
                    onChangeText={(text) => setDescription(text)}
                    value={description}
                    underlineColorAndroid="transparent"
                    multiline={true}
                    autoCapitalize="none"
                />
                 <TextInput
                    style={styles.input}
                    placeholder='Full Name'
                    placeholderTextColor="#aaaaaa"
                    onChangeText={(text) => setPopulation(text)}
                    value={population}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                />
                <TouchableOpacity
                    style={styles.button}  onPress={onAddButtonPress}>
                    <Text style={styles.buttonTitle}>Update</Text>
                </TouchableOpacity>
            </KeyboardAwareScrollView>
        </View>
    )
}